var counter = 0;
var slideNumber = Math.floor(Math.random()*5)+1;


function runSlider(){
	
	changeSlide();
}
function hide(){
	
	 $("#slider").fadeOut(500);
}

function changeSlide(){
	slideNumber++;
	if(slideNumber > 5) slideNumber = 1;
	
	var file = "<img src=\"photo-examples/slide"+ slideNumber +".JPG\"/>";
    $('#slider').html(file);
    
    $("#slider").fadeIn(500);
    
    setTimeout("changeSlide()",5000);
    setTimeout("hide()",4500);
 
}


$(function() {


$("#album a").each(function(){
	$(this).find('span').css("background-image", 'url("'+$(this).attr("href")+'")');
	$(this).addClass("loaded");
})

if($('body#index').length > 0) runSlider();

if($('body#photos').length > 0) baguetteBox.run('#album');

});