# SmartGallery Web application

This application is run in browser to view and manage albums and photos

## Prerequisites

* Web server
* public IP or domain
* server installed

## Run your application

Copy files to public html directory on your http server

TBD

## Repository

[https://gitlab.com/SmartGallery/web](https://gitlab.com/SmartGallery/web)

## Credits

[Bootstrap · The world's most popular mobile-first and responsive front-end framework](http://getbootstrap.com/)

[jQuery](https://jquery.com/)

[GitHub - feimosi/baguetteBox.js: Simple and easy to use lightbox script written in pure JavaScript](https://github.com/feimosi/baguetteBox.js)
